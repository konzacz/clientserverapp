package optionsMenu;

import exceptions.LoginDoesNotExistException;

import static optionsMenu.HandlingMethods.displayUserData;
import static optionsMenu.HandlingMethods.logout;

public class AdminOptionsHandler {

    public static void showAdminOptionsMenu() {

        System.out.println("Available options:\n"
                + "[1] - Display my data\n"
                + "[2] - Modify my data\n"
                + "[3] - Send a message\n"
                + "[4] - Show inbox\n"
                + "[5] - Log out\n"
                + "*** ADDITIONAL ADMIN OPTIONS ***\n"
                + "[6] - Display all users\n"
                + "[7] - Remove the user from database\n"
                + "[8] - Change user account type\n");
    }

    public static void adminCommandHandling(int optionNumber, String login) throws LoginDoesNotExistException {

        switch (optionNumber) {
            case 1 -> displayUserData(login);
//                case 2 -> modifyUserData(login, scanner);
//                case 3 -> sendMessage(login, scanner);
//                case 4 -> showInbox(login);
            case 5 -> {
                logout(login);
            }
            default -> System.out.println("Invalid command.");
        }
    }
}
