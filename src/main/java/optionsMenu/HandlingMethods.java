package optionsMenu;

import exceptions.LoginDoesNotExistException;
import users.User;

import static users.UserUtils.getLoggedInUserByLogin;
import static users.UserUtils.removeUserFromLoggedInUserList;

public class HandlingMethods {

    public static void displayUserData(String login) throws LoginDoesNotExistException {
        User user = getLoggedInUserByLogin(login);
        System.out.printf("My data: \nLogin: [%s]\nPassword: [%s]",
                user.getLogin(), user.getPassword());
    }
     public static void logout(String login) throws LoginDoesNotExistException {
        removeUserFromLoggedInUserList(getLoggedInUserByLogin(login));
        System.out.println("Logged out successfully.");
    }


}
