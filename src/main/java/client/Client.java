package client;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.*;
import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        try (Socket clientSocket = new Socket("localhost", 8080);
             PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))) {

            System.out.println("Server connection successful :)");
            System.out.println("Welcome to the server! \nPlease select an option from the list: " +
                    "\n[1] - Users menu" +
                    "\n[2] - Server menu");

            communicate(out, in);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void communicate(PrintWriter out, BufferedReader in) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String command;
        String response;
        Object decodedResponse;

        do {
            System.out.println("____________________________________________________");
            System.out.print("Type your request ([help] for available commands) --> ");
            command = scanner.next();
            System.out.println("____________________________________________________");
            out.println(command);

            response = in.readLine();
            decodedResponse = decodeCommands(response);

            if (decodedResponse instanceof List<?>) {
                // Obsługa listy
                List<?> commandList = (List<?>) decodedResponse;
                for (Object c : commandList) {
                    // Sprawdzamy, czy każdy element listy jest instancją String przed jego obsługą
                    if (c instanceof String) {
                        System.out.println((String) c);
                    }
                }
            } else if (decodedResponse instanceof String) {
                // Obsługa pojedynczego napisu
                System.out.println((String) decodedResponse);
            }
            if (response == null) {
                System.out.println("Server stop working!");
                break;
            }

        } while (!command.equals("stop"));
    }


    private static Object decodeCommands(String jsonResponse) {
        Gson gson = new Gson();
        try {
            return gson.fromJson(jsonResponse, new TypeToken<List<String>>() {
            }.getType());
        } catch (JsonSyntaxException e) {
            // Jeśli nie uda się zdekodować jako Lista, to zwracamy pojedynczy napis
            return gson.fromJson(jsonResponse, String.class);
        }
    }
}

