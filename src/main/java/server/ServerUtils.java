package server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static server.Server.*;

public class ServerUtils {

    static void serverCommandsHandler(PrintWriter out, Socket clientSocket, String clientMessage) throws IOException {
        switch (clientMessage) {
            case "1" -> gsonResponse(out, getServerUptime());

            case "2" -> gsonResponse(out, getServerInfo());
            case "stop" -> {
                gsonResponse(out,  "Server stop.");
                stop(clientSocket);
            }
            case "help" -> gsonResponse(out, serverInformationCommands());
            default -> gsonResponse(out, "Unknown command. Type 'help' for available commands.");
        }
    }
    private static List<String> serverInformationCommands() {
        List<String> commandList = new ArrayList<>();
        commandList.add("Available commands: ");
        commandList.add("[1] - UPTIME - returns the lifetime of the server");
        commandList.add("[2] - INFO - returns the version number of the server nad the date it was created");
        commandList.add("[stop] - STOP - stops the server and client");
        commandList.add("[help] - for list of available commands");
         return commandList;
    }

    private static List<String> usersMenuCommands() {
        List<String> commandList = new ArrayList<>();
        commandList.add("USERS MENU: ");
        commandList.add("[1] - SIGN UP");
        commandList.add("[2] - LOG IN");
        commandList.add("[0] - BACK - to go back to MAIN MENU");
        commandList.add("--- Please enter the command ---");
        return commandList;
    }

    static void mainMenuCommandsHandler(PrintWriter out, Socket clientSocket, String clientMessage) throws IOException {
        switch (clientMessage) {
            case "1" -> gsonResponse(out, "Option 1 selected.");
            case "2" -> serverCommandsHandler(out, clientSocket, clientMessage);
            case "help" -> gsonResponse(out, mainMenuCommands());
            default -> gsonResponse(out, "Unknown command. Type 'help' for available commands.");
        }
    }
    private static List<String> mainMenuCommands() {
        List<String> commandList = new ArrayList<>();
        commandList.add("MAIN MENU: ");
        commandList.add("[1] - USERS MENU");
        commandList.add("[2] - SERVER MENU");
        commandList.add("[help] - for list of available commands");
        return commandList;
    }




    static void mainMenuCommandsHandler(PrintWriter out, String clientCommand) throws IOException {
        switch (clientCommand) {
            case "1" -> {
                gsonResponse(out, "Main menu");
            }
            case "2" -> {
                gsonResponse(out, "Server menu");
            }
            default -> gsonResponse(out, "Invalid command.");
        }
    }


//    static void handleServerInformationCommands(PrintWriter out, String clientCommand) throws IOException {
//
////        int optionNumber = readInt(in);
//
//        switch (clientCommand) {
//            case "1" -> gsonResponse(out, getServerUptime());
//            case "2" -> gsonResponse(out, getServerInfo());
////                case 3 -> {
////                    gsonResponse(out, "Server stop.");
////                    stop();
////                }
//            case "4" -> gsonResponse(out, serverInformationCommands());
//            case BACK_TO_MAIN_MENU -> {
//                return;
//            }
//            default -> gsonResponse(out, "Invalid option number. Type [4] for available commands.");
//        }
//    }


    private static String getServerInfo() {
        String version = "1.0.0";
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");
        LocalDateTime creationDate = LocalDateTime.now();
        return "Server version: " + version + ", Created on: " + creationDate.format(dtf);
    }

    private static String getServerUptime() {
        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(SERVER_RUNTIME, now);
        return "Server is running for: " + durationFormat(duration);
    }

    private static String durationFormat(Duration duration) {
        return String.format("%02d:%02d:%02d", duration.toHours(), duration.toMinutes(), duration.toSecondsPart());
    }

    static void gsonResponse(PrintWriter out, List<String> responseList) {
        String jsonResponse = gson.toJson(responseList);
        out.println(jsonResponse);
    }

    static void gsonResponse(PrintWriter out, String responseString) {
        String jsonResponse = gson.toJson(responseString);
        out.println(jsonResponse);
    }



//    public static void printMainCommands(PrintWriter out) {
//        gsonResponse(out, mainCommands());
//    }
//
//    public static void printServerInformationCommands(PrintWriter out) {
//        gsonResponse(out, serverInformationCommands());
//    }


}
