package server.managing;

import mailbox.Mailbox;
import users.User;

import users.UserUtils;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.PrintWriter;

import static users.UserUtils.whetherLoginAlreadyExist;


public class Manager {

    private void registrationAccountAsAdmin(BufferedReader in, PrintWriter out) throws IOException {
        String login = in.readLine();
        String password = in.readLine();
        createNewAccountAsAdmin(login, password);
        out.println("Registration successful.\n" +
                "Admin: [Login]: " + login + "[Password]: " + password);
    }

    private void registrationAccountAsBasicUser(BufferedReader in, PrintWriter out) throws IOException {
        String login = getLogin(out, in);
        String password = getPassword(out, in);
        createNewAccountAsBasicUser(login, password);
        out.println("Registration successful.\n" +
                "Admin: [Login]: " + login + "[Password]: " + password);
    }

    private void createNewAccountAsAdmin(String login, String password) {
        UserUtils userUtils = new UserUtils();
        Mailbox mailbox = new Mailbox();
        User admin = userUtils.createNewAdmin(login, password, mailbox);
        userUtils.addUserToList(admin);
        System.out.println("A new administrator account has been created.");
    }

    private void createNewAccountAsBasicUser(String login, String password) {
        UserUtils userUtils = new UserUtils();
        Mailbox mailbox = new Mailbox();
        User basicUser = userUtils.createNewBasicUser(login, password, mailbox);
        userUtils.addUserToList(basicUser);
        System.out.println("A new basic user account has been created.");
    }

    public String getLogin(PrintWriter out, BufferedReader in) throws IOException {
        String login;
        boolean whetherLoginAlreadyExist;
        String enterLoginLabel = "Enter login: ";
        out.println(enterLoginLabel);
        do {
            login = in.readLine();
            whetherLoginAlreadyExist = whetherLoginAlreadyExist(login);
        } while (whetherLoginAlreadyExist);
        return login;
    }

    public String getPassword(PrintWriter out, BufferedReader in) throws IOException {
        String enterPasswordLabel = "Enter password: ";
        return in.readLine();
    }

    private void handleExistingLogin(boolean whetherLoginAlreadyExist, PrintWriter out, String login) {
        String message;
        if (whetherLoginAlreadyExist) {
            message = String.format("The user with login: [%s] already exists!", login);
        } else {
            message = "Login entered: " + login;
        }
        out.println(message);
    }

}
