package server.managing;

import exceptions.LoginDoesNotExistException;

import java.util.Scanner;

import static optionsMenu.AdminOptionsHandler.adminCommandHandling;
import static optionsMenu.AdminOptionsHandler.showAdminOptionsMenu;
import static optionsMenu.BasicUserOptionsHandler.basicUserCommandHandling;
import static optionsMenu.BasicUserOptionsHandler.showBasicUserOptionsMenu;

import static users.UserUtils.*;

public class HandleLoggedInUser {

    private static void handleLoggedInUser(String login, Scanner scanner) throws LoginDoesNotExistException {
        loggedInUserList.add(getUserByLogin(login));
        boolean whetherUserIsAdmin = getUserByLogin(login).isAdmin();

        if (whetherUserIsAdmin) {
            showAdminOptionsMenu();
        } else {
            showBasicUserOptionsMenu();
        }
        while (true) {
            int optionNumber = scanner.nextInt();
            scanner.nextLine();
            if (whetherUserIsAdmin) {
                adminCommandHandling(optionNumber, login);
            } else {
                basicUserCommandHandling(optionNumber, login);
            }
        }
    }
}
