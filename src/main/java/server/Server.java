package server;

import com.google.gson.Gson;

import java.net.*;
import java.io.*;
import java.time.LocalDateTime;

import static server.ServerUtils.*;

public class Server {

    static final LocalDateTime SERVER_RUNTIME = LocalDateTime.now();

    static ServerSocket serverSocket;
    final static Gson gson = new Gson();


    public static void main(String[] args) throws Exception {
        serverSocketInit();
        sendResponse();
    }

    private static void serverSocketInit() throws IOException {
        serverSocket = new ServerSocket(8080);
        System.out.println("Server is running on port: 8080");

    }

    private static void sendResponse() throws IOException {
        try (Socket clientSocket = serverSocket.accept();
             PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))) {
            System.out.println("Client connected");

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println("Client: " + inputLine);
                serverCommandsHandler(out, clientSocket, inputLine);
                if (inputLine.equals("stop")) {
                    break;
                }
            }
            System.out.println("PrintWriter closed.");
            System.out.println("BufferedReader closed.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void stop(Socket clientSocket) {
        try {
            if (serverSocket != null && !serverSocket.isClosed()) {
                serverSocket.close();
                System.out.println("Server socket closed.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (clientSocket != null && !clientSocket.isClosed()) {
                clientSocket.close();
                System.out.println("Client socket closed.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}






