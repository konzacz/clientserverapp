package exceptions;

public class LoginDoesNotExistException extends Exception {

    public LoginDoesNotExistException(String message) {
        super(message);
    }
}
