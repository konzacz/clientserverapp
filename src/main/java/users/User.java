package users;

import mailbox.Mailbox;

import static users.UserType.ADMIN;

enum UserType {
    ADMIN,
    BASIC_USER
}

public class User {
    private String login;
    private String password;
    private UserType userType;
    private Mailbox mailbox;

    public User(String login, String password, UserType userType, Mailbox mailbox) {
        this.login = login;
        this.password = password;
        this.userType = userType;
        this.mailbox = mailbox;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public UserType getAuthorization() {
        return userType;
    }

    public void setAuthorization(UserType authorization) {
        this.userType = authorization;
    }

    public Mailbox getMailbox() {
        return mailbox;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return userType.equals(ADMIN);
    }

    @Override
    public String toString() {
        return "Login: " + login + "| "
                + "Authorization: " + userType + "| "
                + "Mailbox: " + mailbox.getNumberOfAllMessages();
    }
}
