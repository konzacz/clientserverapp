package users;

import exceptions.LoginDoesNotExistException;
import mailbox.Mailbox;

import java.util.ArrayList;
import java.util.List;

import static users.UserType.ADMIN;
import static users.UserType.BASIC_USER;


public class UserUtils {

    public static final List<User> usersList = new ArrayList<>();
    public static final List<User> loggedInUserList = new ArrayList<>();

    public User createNewBasicUser(String login, String password, Mailbox mailbox) {
        return new User(login, password, BASIC_USER, mailbox);
    }

    public User createNewAdmin(String login, String password, Mailbox mailbox) {
        return new User(login, password, ADMIN, mailbox);
    }

    public void addUserToList(User user) {
        usersList.add(user);
    }
    public void addUserToLoggedInUserList(User user) { loggedInUserList.add(user);}

    public static void removeUserFromList(User user, User loggedInUser) {
        if (loggedInUser.isAdmin()) {
            usersList.remove(user);
        } else {
            System.out.println("No authority to delete user.");
        }
    }

    public static void removeUserFromLoggedInUserList(User loggedInUser) {
            loggedInUserList.remove(loggedInUser);
    }


    public void changeUserTypeToAdmin(User user, User loggedInUser) {
        if (loggedInUser.isAdmin()) {
            user.setAuthorization(ADMIN);
        } else {
            System.out.println("No authority to change authorizarion.");
        }
    }

    public void changeUserTypeToBasicUser(User user, User loggedInUser) {
        if (loggedInUser.isAdmin()) {
            user.setAuthorization(BASIC_USER);
        } else {
            System.out.println("No authority to change authorizarion.");
        }
    }

    public static User getUserByLogin(String login) throws LoginDoesNotExistException {
        return gettingUserByLogin(login, usersList);
    }

    public static User getLoggedInUserByLogin(String login) throws LoginDoesNotExistException {
        return gettingUserByLogin(login, loggedInUserList);
    }

    public static List<User> getAllUsers(User loggedInUser) {
        if (loggedInUser.isAdmin()) {
            return usersList;
        } else {
            System.out.println("No viewing privileges for all users.");
            return new ArrayList<>();
        }
    }


    public static boolean isUserPasswordValid(String login, String password) {
        for (User user : usersList) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public static boolean whetherLoginAlreadyExist(String login) {
        for (User user : usersList) {
            if (user.getLogin().equals(login)) {
                return true;
            }
        }
        return false;
    }

    public static boolean whetherUserDatabaseIsEmpty() {
        return usersList.isEmpty();
    }

    public static User gettingUserByLogin(String login, List<User> list) throws LoginDoesNotExistException {
        return list.stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst()
                .orElseThrow(() -> new LoginDoesNotExistException("Login does not exist."));
    }
}
