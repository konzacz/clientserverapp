package mailbox;

public class Mailbox {

    ReadMessagesBox readMessages;
    UnreadMessagesBox unreadMessages;

    public int getNumberOfAllMessages() {
        return readMessages.getNumberOfReadMessages()
                + unreadMessages.getNumberOfUnReadMessages();
    }
}
