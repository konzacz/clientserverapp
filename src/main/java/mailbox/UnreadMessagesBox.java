package mailbox;

import java.util.ArrayList;
import java.util.List;

public class UnreadMessagesBox {
    public static final int MAX_CAPACITY = 5;
    private final ArrayList<Message> unreadMessagesBox;


    public UnreadMessagesBox() {
        unreadMessagesBox = new ArrayList<>();
    }

    public List<Message> getUnreadMessages() {
        return unreadMessagesBox;
    }

    public int getNumberOfUnReadMessages() {
        return unreadMessagesBox.size();
    }

    public String getMessageById(int id) {
        return unreadMessagesBox.get(id).toString();
    }

    public static int getMaxCapacity() {
        return MAX_CAPACITY;
    }
}
