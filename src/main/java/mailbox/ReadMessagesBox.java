package mailbox;

import java.util.ArrayList;
import java.util.List;

public class ReadMessagesBox {

    private final ArrayList<Message> readMessagesBox;


    public ReadMessagesBox() {
        readMessagesBox = new ArrayList<>();
    }

    public int getNumberOfReadMessages() {
        return readMessagesBox.size();
    }

    public List<Message> getReadMessages() {
        return readMessagesBox;
    }

    public String getMessageById(int id) {
        return readMessagesBox.get(id).toString();
    }

    public void removeMessageFromRead(int index) {
        readMessagesBox.remove((index - 1 ));
    }

    public void clearAllMessagesFromInbox() {
        readMessagesBox.clear();
    }

}
